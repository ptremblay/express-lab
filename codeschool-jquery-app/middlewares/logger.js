/**
 * Created by Patrick on 2015-03-02.
 */
'use strict';

module.exports = function(req,res,next){
    var start = +new Date();
    var stream = process.stdout;
    var url = req.url;
    var method = req.method;

    res.on('finish', function(){
        var duration = +new Date() - start;
        var message = method + ' to ' + url + '\ntook ' + duration + ' ms\n\n';
        stream.write(message);
    });

    next();
}