'use strict';

var express = require('express');
var app = express();

var logger = require('./middlewares/logger');

app.use(logger);

app.get('/', function(req,res){
    res.sendFile(__dirname +'/public/index.html');
});

app.use(express.static('public'));

var blocks = require('./routes/blocks');
app.use('/blocks',blocks);

app.listen(3000, function(){
    console.log('Listening on port %d',3000);
});